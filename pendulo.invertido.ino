#include <PID_v1.h>

// Defina os pinos de controle dos motores
const int motor1PWM = 6;
const int motor1DirA = 8;
const int motor1DirB = 7;

const int motor2PWM = 11;
const int motor2DirA = 10;
const int motor2DirB = 9;

// Pino do potenciômetro
const int potPin = A0;

// Variáveis PID
double setpoint = 642;  // Valor desejado
double input, output;

// Parâmetros PID (ajuste conforme necessário)
double Kp = 1;
double Ki = 0.5;
double Kd = 0.05;

PID myPID(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);

void setup() {
  // Configuração dos pinos
  pinMode(motor1PWM, OUTPUT);
  pinMode(motor1DirA, OUTPUT);
  pinMode(motor1DirB, OUTPUT);

  pinMode(motor2PWM, OUTPUT);
  pinMode(motor2DirA, OUTPUT);
  pinMode(motor2DirB, OUTPUT);

  // Inicialização do PID
  myPID.SetMode(AUTOMATIC);

  // Inicialização da comunicação serial para imprimir os valores
  Serial.begin(9600);
}

void loop() {
  // Leitura do valor do potenciômetro
  int potValue = analogRead(potPin);
  
  // Mapeia o valor do potenciômetro para o intervalo desejado
  //setpoint = map(potValue, 0, 1023, -100, 100);

  // Imprime os valores do potenciômetro
  Serial.print("Potentiometer Value: ");
  Serial.println(potValue);

  // Atualização do PID
  input = readPot(); 
  myPID.Compute();

  // Imprime os valores PID
  Serial.print("PID Input: ");
  Serial.print(input);
  Serial.print(", PID Output: ");
  Serial.println(output);
  delay(1000);

  // Saída do PID controla a velocidade dos motores
  driveMotors(output);
}

int readPot() 
{
  // Leitura do potenciômetro (pode precisar ser ajustado dependendo do potenciômetro)
  return analogRead(potPin);
}

void driveMotors(int speed) {
  // Controla a direção e velocidade do motor 1
  if (input<= 592) {
    analogWrite(motor1PWM, (-speed * -1));
    digitalWrite(motor1DirA, LOW);
    digitalWrite(motor1DirB, HIGH);

    analogWrite(motor2PWM, (-speed * -1));
    digitalWrite(motor2DirA, LOW);
    digitalWrite(motor2DirB, HIGH);
    
  } else if(input >= 684) {
    analogWrite(motor1PWM, (speed * -1));
    digitalWrite(motor1DirA, HIGH);
    digitalWrite(motor1DirB, LOW);


    analogWrite(motor2PWM, (speed * -1));
    digitalWrite(motor2DirA, HIGH);
    digitalWrite(motor2DirB, LOW );
    
  } else{
    analogWrite(motor1PWM, 0);
    digitalWrite(motor1DirA, LOW);
    digitalWrite(motor1DirB, LOW);


    analogWrite(motor2PWM, 0);
    digitalWrite(motor2DirA, LOW);
    digitalWrite(motor2DirB, LOW);
  }

}